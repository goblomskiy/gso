
theme: /
    @InputText
        {
          "prompt" : "Whats is your name?",
          "varName" : "text",
          "then" : "/newNode_1"
        }
    state: newNode_01
        a: Name yourself, please

        state: CatchText
            q: *
            script:
                $session.text = $parseTree.text;
            go!: /newNode_1
    @InputNumber
        {
          "prompt" : "How old are you?",
          "varName" : "number",
          "minValue" : 1,
          "maxValue" : 100,
          "failureMessage" : [
            "Enter number from 5 to 100",
            "Enter positive number"
          ],
          "then" : "/newNode_2"
        }
    state: newNode_1
        a: Enter your age, please

        state: CatchNumber
            q: $Number
            script:
                var failureMessages = [
            "Enter number from 5 to 100",
            "Enter positive number"
                ];
                var failureRandom = failureMessages[$reactions.random(failureMessages.length)];
                if ($parseTree._Number < 1) {
                    $reactions.answer(failureRandom);
                } else
                if ($parseTree._Number > 100) {
                    $reactions.answer(failureRandom);
                } else
                {
                    $session.number = $parseTree._Number;
                    $temp.number_ok = true;
                }
            if: $temp.number_ok
                go!: /newNode_2
            else:
                go: CatchNumber

        state: CatchAll
            q: *
            go!: ..
    @Confirmation
        {
          "prompt" : "Please confirm to process your personal data?",
          "agreeState" : "/newNode_5",
          "disagreeState" : "/newNode_4",
          "useButtons" : true,
          "agreeButton" : "Agree",
          "disagreeButton" : "No"
        }
    state: newNode_2
        a: Do you confirm to process your personal data?
        buttons:
            "Agree" -> Agree
            "No" -> Disagree
        state: Agree
            q: $agree
            go!: /newNode_5
        state: Disagree
            q: $disagree
            go!: /newNode_4

    state: newNode_4
        a: Goodbye!
        buttons:
            "Let's start from the beginning" -> /newNode_6

    state: newNode_5
        a: Thank you! {{$session.text}}, you are {{$session.number}} year old 

    state: newNode_6
        a: Are you sure to start again?
        image: https://248305.selcdn.ru/public_test/sadmin/tXB7NC98ssjJPLgo.jpg
        buttons:
            "Yes" -> /newNode_01
            "No" -> /newNode_4
